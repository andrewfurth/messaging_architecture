$( document ).ready(function() {
  initialize()
  
});

var initialize = function() {
  initializeStarling()
  initializeFirebase()
  // initializePusher()
  initializePubnub()
  initializeAbly()
}

var initializeStarling = function() {
  var form = $("#messageForm")
  form.submit(captureNewStarlingData)
}

var captureNewStarlingData = function(e) {
  e.preventDefault()
  var input = $("#messageText")
  console.log("Submitted text TO captureNewStarlingData: ", input.val())
  var newData = input.val()
  $('body').trigger('starling-data', [newData])
  input.val('')
}


var initializeFirebase = function() {
  var config = {
    apiKey: "AIzaSyD3Xs78ik_iCW_fKB5lmug7zvtyNwRdiO4",
    authDomain: "messaging-architecture-f9e75.firebaseapp.com",
    databaseURL: "https://messaging-architecture-f9e75.firebaseio.com",
    storageBucket: "messaging-architecture-f9e75.appspot.com",
    messagingSenderId: "264845585119"
  };
  
  firebase.initializeApp(config);

  // You can retrieve services via the firebase variable...
  var firebaseDatabase = firebase.database();

  var firebaseTransmitDataToServer = function(ev, data) {
    console.log("Sending data to server through Firebase: ", data)
    // Push new message up to 'starling-data' db
    firebaseDatabase.ref('starling-data').push({
      text: data
    })
  }

  var firebaseReceiveDataFromServer = function(data) {
    console.log("Receiving data on server from Firebase: ", data)
    $msg = $('<div>Ack: ' + data + ' </div>')
    $('#Firebase .panel-body').append($msg)
  }

  // Remove 'ack' DB on client load
  firebaseDatabase.ref('ack').set({})

  // Listen for new entries to the ack table
  firebaseDatabase.ref('ack').on("child_added", function(snapshot) {
    console.log("last child added to 'ack': ", snapshot.val());  // Should only be one entry, b/c we're only grabbing the last child
    firebaseReceiveDataFromServer(snapshot.val().text)
  }, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
  });

  $('body').on('starling-data', firebaseTransmitDataToServer)
}

var initializePusher = function() {

  var pusherTransmitDataToServer = function(ev, data) {
    console.log("Sending data to server through Pusher: ", data)
    // console.log("privateChannel: ", privateChannel)
    privateChannel.trigger('client-starling-data', {"message": data})
  }

  var pusherReceiveDataFromServer = function(data) {
    console.log("Receiving data on server from Pusher: ", data)
    $msg = $('<div>Ack: ' + data + ' </div>')
    $('#Pusher .panel-body').append($msg)
  }

  
  $('body').on('starling-data', pusherTransmitDataToServer)

  // Enable pusher logging - don't include this in production
  // Pusher.logToConsole = true;

  var pusher = new Pusher("be9c08337a3ca4ac4bc0", {
    encrypted: true,
    authEndpoint: '/pusher/auth'
  });

  var privateChannel = pusher.subscribe('private-test-channel');

  privateChannel.bind('pusher:subscription_succeeded', function(data) {
    console.log("CLIENT subscription_succeeded!!!!!!!")
    privateChannel.bind('my-event', function(data) {
      alert('An event was triggered with message: ' + data.message);
    });
    privateChannel.bind('starling-data', function(data) {
      console.log("Data from Pusher", data)
    })
    privateChannel.bind('ack', function(data) {
      pusherReceiveDataFromServer(data.message)
      
    })
  });
}

var initializePubnub = function() {
  console.log("Pubnub initialized")
  

  var pubnubTransmitDataToServer = function(ev, data) {
    console.log("Sending data to server through PubNub: ", data)
    pubnub.publish({
      // key of 'publish' method MUST be 'message'
      message: {
        dataFromClient: data
      },
      channel: 'client_to_server'
    })
  }

  var pubnubReceiveDataFromServer = function(data) {
    console.log("Receiving data on server from PubNub: ", data)
    $msg = $('<div>Ack: ' + data + ' </div>')
    $('#Pubnub .panel-body').append($msg)
  }
  
  $('body').on('starling-data', pubnubTransmitDataToServer)

  var pubnub = new PubNub({
    subscribeKey: 'sub-c-ee4c5b94-c3c4-11e6-972b-0619f8945a4f',
    publishKey: 'pub-c-8728cf1d-e22a-4595-9924-f7073b5ad7c8',
    ssl: true
  })

  pubnub.addListener({
    message: function(message){
      console.log("PubNub message from server: ", message)
      pubnubReceiveDataFromServer(message.message.dataFromServer)
    }
  })

  pubnub.subscribe({
      channels: ['server_to_client']
  });


}

var initializeAbly = function() {
  var ablyRealtime = new Ably.Realtime({ authUrl: '/ably/auth' });

  ablyRealtime.connection.once('connected', function() {
    var user = ablyRealtime.auth.tokenDetails.clientId || 'anonymous';
    var capability = ablyRealtime.auth.tokenDetails.capability;
    console.log(
      'You are now connected to Ably \n' +
      'User: ' + user + ' \n' +
      'Capabilities: ' + capability
    );
  });

  var starlingDataChannel = ablyRealtime.channels.get("starling-data");
  var ackChannel = ablyRealtime.channels.get("ack");

  var ablyTransmitDataToServer = function(ev, data) {
    console.log("Sending data to server through Ably: ", data)
    starlingDataChannel.publish("send starling data  to client", { "text": data });
  }

  ackChannel.subscribe(function(msg) {
    console.log("Ack Received: " + JSON.stringify(msg.data));
    var text = msg.data.text
    ablyReceiveDataFromServer(text)
  });

  var ablyReceiveDataFromServer = function(data) {
    console.log("Receiving data on server from Ably: ", data)
    $msg = $('<div>Ack: ' + data + ' </div>')
    $('#Ably .panel-body').append($msg)
  }

  $('body').on('starling-data', ablyTransmitDataToServer)
}



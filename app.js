const express = require('express');
const path = require('path');
const FirebaseAdmin = require("firebase-admin");
const Pusher = require('pusher');
const PubNub = require('pubnub');
const Ably = require('ably')
const bodyParser = require('body-parser');

var firebaseServiceAccount = require("./firebaseServiceAccountKey.json");

var pusherApp_id = "280883"
var pusherKey = "be9c08337a3ca4ac4bc0"
var pusherSecret = "cc05ec641fca4bfd283f"

const app = express()
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// serve our static stuff
app.use(express.static(path.join(__dirname, 'public')))

//***************************************************************************
// Firebase
//***************************************************************************
FirebaseAdmin.initializeApp({
  credential: FirebaseAdmin.credential.cert(firebaseServiceAccount),
  databaseURL: "https://messaging-architecture-f9e75.firebaseio.com/"
});


var db = FirebaseAdmin.database();
var starlingDataRef = db.ref("starling-data");
var ackRef = db.ref("ack");

// Logic comes from: https://stackoverflow.com/questions/19883736/how-to-discard-initial-data-in-a-firebase-db/27693310#27693310
var ignoreItems = true;

// Listen for new children being added to Starling Data
starlingDataRef.on("child_added", function(snapshot) {
  if (!ignoreItems) {
    console.log("Last child added to Starling Data: ", snapshot.val());
    // Push the last message in 'starling-data' into 'ack'
    ackRef.push(snapshot.val())
  }
}, function (errorObject) {
  console.log("The read failed: " + errorObject.code);
});

starlingDataRef.once('value', function(snapshot) {
  ignoreItems = false;
})

//***************************************************************************
// Pusher
//***************************************************************************
var pusher = new Pusher({
  appId: pusherApp_id,
  pusherKey: pusherKey,
  pusherSecret: pusherSecret,
  encrypted: true
});

// pusher.trigger('private-test-channel', 'my-event', {"message": "hello world"});

app.post('/pusher/auth', function(req, res) {
  var socketId = req.body.socket_id;
  var channel = req.body.channel_name;
  console.log("socketId: " + socketId + " and channel: " + channel)
  var auth = pusher.authenticate(socketId, channel);
  console.log("AUTH", auth)
  res.send(auth);
});

app.post('/callbacks/pusher', function(req, res) {
  // If this console.log isn't working, run 'ngrok http 3000' in a new terminal tab
  // and add the 'Forwarding' URL that maps to localhost:3000 to the webhooks tab in Pusher dashboard
  // with a '/callbacks/pusher' suffix (i.e. http://d9059e69.ngrok.io/callbacks/pusher)
  console.log("Pusher Callbacks REQ", req.body.events)
  var data = JSON.parse(req.body.events[0].data)
  pusher.trigger('private-test-channel', 'ack', {"message": data.message})
  res.sendStatus(200)
});


//***************************************************************************
// PubNub
//***************************************************************************
  var pubnub = new PubNub({
    subscribeKey: 'sub-c-ee4c5b94-c3c4-11e6-972b-0619f8945a4f',
    publishKey: 'pub-c-8728cf1d-e22a-4595-9924-f7073b5ad7c8'
  })

  pubnub.addListener({
    // key of 'addListener' method MUST be 'message'
    message: function(message){
      console.log("PubNub message from client: ", message.message.dataFromClient)
      // Send message back to the client on different channel
      pubnub.publish({
      message: {
        dataFromServer: message.message.dataFromClient
      },
      channel: 'server_to_client'
    })
    }
  })

  pubnub.subscribe({
    channels: ['client_to_server']
  });

  // pubnub.publish({
  //   message: {
  //       "color" : "blue"
  //   },
  //   channel: 'demo_tutorial'
  // });

//***************************************************************************
// Ably
//***************************************************************************
// Selecting the right type of auth: https://www.ably.io/documentation/general/authentication#selecting-auth

const ablyApiKey = 'RyaGVw.MqcR5A:_wewQ5kcB7y_QYTb'
var ablyRealtime = new Ably.Realtime({ key: ablyApiKey });
var ablyRest = new Ably.Rest({key: ablyApiKey})

app.get('/ably/auth', (req, res) => {
  var tokenParams = {
    'capability': { '*': ['publish', 'subscribe'] },
    'clientId': 'testUser'
  }
  ablyRest.auth.createTokenRequest(tokenParams, function(err, tokenRequest) {
    if (err) {
      res.status(500).send("Ably - Error requesting token: " + JSON.stringify(err));
    } else {
      res.setHeader('Content-Type', 'application/json')
      res.send(JSON.stringify(tokenRequest))
    }
  })
})

var starlingDataChannel = ablyRealtime.channels.get("starling-data");
var ackChannel = ablyRealtime.channels.get("ack");

starlingDataChannel.subscribe(function(msg) {
  console.log("Starling Data Received: " + JSON.stringify(msg.data));
  var msg = msg.data.text
  // Send data back over 'ack' channel
  ackChannel.publish("send message back to client", { "text": msg });
});

app.get('*', function (req, res) {
  console.log("Server requested")
  res.sendFile('index.html', {root: 'public'})
})

const PORT = process.env.PORT || 3000
app.listen(PORT, function() {
  console.log('node server running at port ' + PORT)
})

module.exports = app;
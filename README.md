# Messaging Architecture #

This application is designed to set-up and test the major messaging platforms and see which will be the best fit for VersaMe

### How do I get set up? ###
* `git clone git@bitbucket.org:andrewfurth/messaging_architecture.git`
* `npm install`
+ Run `nodemon app.js` to spin up the local server on `localhost:3000`


For Pusher, you will need to configure a webhook where you can trigger messages from the client. The messages will submit a POST request to the URL you've specified in the webhook, and then you can access their content in the server.

* Install Ngrok
* Create a `bin` folder in your home directory, unzip Ngrok and move it into that folder
* Update your PATH in your `.profile` or `.bashrc` to include the `bin` folder ([Directions](https://askubuntu.com/questions/402353/how-to-add-home-username-bin-to-path))
+ In a separate tab in your console, run `ngrok http 3000`
    * Note the URL that maps to the `http` address for localhost:3000 ![Screen Shot 2016-12-15 at 7.13.47 PM.png](https://bitbucket.org/repo/x4KRzB/images/1726979521-Screen%20Shot%202016-12-15%20at%207.13.47%20PM.png)
+ Log in to Pusher
    * Email: `andrew@versame.com`
    * Password: `versame`

* Go to *Your Apps* >> *messaging_architecture* >> *Webhooks*
* If the URL from your Ngrok terminal tab is different than the webhook(s) listed, add the new URL `[NgrokURL]/callbacks/pusher` and ensure that webhook is enabled (you may have to disable the others, I'm not sure ![Screen Shot 2016-12-15 at 7.18.11 PM.png](https://bitbucket.org/repo/x4KRzB/images/968169162-Screen%20Shot%202016-12-15%20at%207.18.11%20PM.png)
* Be aware that if your node server shuts down, Ngrok may stop and need to be restarted. If that happens, a new URL may be generated and need to be re-added to the Pusher dashboard.